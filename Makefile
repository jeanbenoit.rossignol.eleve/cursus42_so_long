#--PROJECT_NAME--#
PROJECT_NAME	= so_long

#--C FILES--# > all .c files
C_FILES			= exit_message.c flood_fill.c free_utils.c get_map.c get_map_utils.c map_valid.c moves_utils.c moves_utils_1.c set_data.c so_long.c start_game.c

#--C FILES TO O FILES--# > where you make the .o files dependencies
O_FILES			= $(C_FILES:.c=.o)

#--NAME--# > name of the project
NAME			= $(PROJECT_NAME)

#--COMMANDS--# > all the bash command you will use in the Makefile
GCC				= gcc
MKDIR			= mkdir
RM				= rm
NORMINETTE		= norminette

#--FLAGS--# > flags used by the command above
ERROR_FLAGS		= -Werror -Wall -Wextra
P_FLAG			= -p
O_FLAG			= -o
C_FLAG			= -c
RF_FLAG			= -rf
F_FLAG			= -f
I_FLAG			= -I
DEBUG_FLAG		= -g
MLX_FLAG		= -lmlx -framework OpenGL -framework Appkit
MAKE_FLAG		= -C
LIBFT_FLAG		= -L./libft -lft

#--DIR PATH--# > path to the file
SRC_DIR			= src/
OBJ_DIR			= obj/
INC_DIR			= include/
LIBFT_DIR		= libft/

#--PREFIX--#
PRE_SRC			= $(addprefix $(SRC_DIR), $(C_FILES))
PRE_OBJ			= $(addprefix $(OBJ_DIR), $(O_FILES))

#--VPATH--#
VPATH			= $(SRC_DIR)

#--ACTIONS--# > all the thing you want your Makefile to do
$(OBJ_DIR)%.o:		%.c
				@$(MKDIR) $(P_FLAG) $(OBJ_DIR)
				@$(GCC) $(DEBUG_FLAG) $(ERROR_FLAGS) $(I_FLAG) $(INC_DIR) $(O_FLAG) $@ $(C_FLAG) $<

$(NAME):			$(PRE_OBJ)
				@echo "Compiling $(PROJECT_NAME)..."
				@$(MAKE) $(MAKE_FLAG) $(LIBFT_DIR)
				@$(GCC) $(DEBUG_FLAG) $(ERROR_FLAGS) $(MLX_FLAG) $(PRE_OBJ) $(O_FLAG) $(NAME) $(LIBFT_FLAG)
				@echo "Compiling $(PROJECT_NAME) done."

all:				$(NAME)

clean:
				@echo "Removing $(PROJECT_NAME) object files..."
				@$(MAKE) $(MAKE_FLAG) $(LIBFT_DIR) clean
				@$(RM) $(F_FLAG) $(PRE_OBJ)
				@$(RM) $(RF_FLAG) $(OBJ_DIR)
				@echo "Removing $(PROJECT_NAME) object files done."

fclean:				clean
				@echo "Removing $(PROJECT_NAME) program..."
				@$(MAKE) $(MAKE_FLAG) $(LIBFT_DIR) fclean
				@$(RM) $(F_FLAG) $(NAME)
				@echo "Removing $(PROJECT_NAME) program done."

re:					fclean all

norminette:
				@$(NORMINETTE) $(SRC_DIR) $(INC) $(LIBFT_DIR)

.PHONY: all fclean clean re norminette 
