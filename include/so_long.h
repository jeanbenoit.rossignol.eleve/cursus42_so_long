/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:21:14 by jrossign          #+#    #+#             */
/*   Updated: 2022/10/05 11:06:20 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H

# include "../libft/include/libft.h"
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include "/usr/local/include/mlx.h"

# define MAP_EXTENSION ".ber"
# define VALID_CHAR "01CEP"
# define ERROR_BACKSLASH "Error\n"
# define MAP_SPACE "The map has spaces in it.\n"
# define MAP_NO_FOUND "Map file not found.\n"
# define OTHER_CHAR "Map contain different char.\n"
# define MAP_NOT_BER "Map is not .ber.\n"
# define MAP_NOT_CLOSE "The map is not closed.\n"
# define MAP_NULL "Map does not exist anymore...\n"
# define MAP_NO_START "The map does not have a starting point.\n"
# define MAP_NO_EXIT "The map does not have an exit.\n"
# define MAP_NO_COLL "The map does not have a collectible.\n"
# define MAP_NOT_RECTANGLE "The map is not a rectangle.\n"
# define MAP_NOT_OPEN "The map can not be completed.\n"
# define MAP_TOO_MANY_P "The map has more than one starting point.\n"
# define MAP_IS_DIR "The map is a directory.\n"
# define MAP_TOO_MANY_E "The map has more than one exit.\n"
# define MAP_EMPTY "The file is empty.\n"

# define COLL "asset/coin.xpm"
# define EXIT "asset/exit.xpm"
# define HERO "asset/player.xpm"
# define WALL "asset/wall.xpm"
# define FLOOR "asset/floor.xpm"
# define IMG_SIZE 32

typedef struct s_data
{
	int				width;
	int				height;
	int				coll_amount;
	int				move;
	int				player_position[2];
	char			**map;
	char			**map_copy;
	struct s_mlx	*mlx_data;
}					t_data;

typedef struct s_mlx
{
	int		img_s;
	void	*mlx;
	void	*mlx_window;
	void	*coll;
	void	*exit;
	void	*wall;
	void	*floor;
	void	*player;
}			t_mlx;

//exit_message.c
void	exit_message(char *error);
void	exit_message_free(t_data *data, char *error);
void	exit_message_char(t_data *data, char c);
int		exit_free(t_data *data);

//flood_fill.c
void	flood_fill(t_data *data);
void	flood_fill_algo(t_data *data, int x, int y);
void	check_exit(t_data *data, int x, int y);
void	check_after_fill(t_data *data);
void	copy_map(t_data *data);

//free_utils.c
void	free_data(t_data *data);
void	free_map_copy(t_data *data);

//get_map.c
void	check_space(char *line);
void	check_dir(char *map_name);
char	**get_map(char *map_name);

//get_map_utils.c
int		check_str(char *line);
void	check_valid_extension(char *file_name);
void	get_coll_amount(t_data *data);
char	*strjoin_free(char *s1, char *s2);

//map_valid.c
void	check_map_valid(t_data *data);
void	check_valid_char(t_data *data);
void	check_char(t_data *data, char c);
void	check_map_rectangle(t_data *data);
void	check_map_closed(t_data *data);

//moves_utils.c
void	move_up(t_data *data);
void	move_down(t_data *data);
void	move_right(t_data *data);
void	move_left(t_data *data);

//moves_utils_1.c
void	set_new_pos(t_data *data, int pos_x, int pos_y);
void	set_coll(t_data *data, int pos_x, int pos_y);
void	handle_exit(t_data *data);

//set_data.c
void	set_data(char *map_name, t_data *data);
void	find_player(t_data *data);

//start_game.c
int		move_key(int keycode, t_data *data);
void	start_game(t_data *data);
void	set_mlx(t_data *data);
void	set_window(t_data *data);
void	add_img(t_mlx *mlx, char c, int x, int y);

#endif
