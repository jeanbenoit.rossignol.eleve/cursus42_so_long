/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:16:42 by jrossign          #+#    #+#             */
/*   Updated: 2022/09/30 17:59:37 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

int	check_str(char *line)
{
	int	i;

	i = 0;
	if (!line)
		return (1);
	while (line[i])
	{
		if (line[i] == '\n')
			return (0);
		i++;
	}
	return (1);
}

void	check_valid_extension(char *file_name)
{
	int		length;
	char	*extension;

	if (!file_name)
		exit_message(MAP_NULL);
	length = ft_strlen(file_name);
	extension = ft_substr(file_name, length - 4, length);
	if (ft_strncmp(extension, MAP_EXTENSION, 4))
	{
		free(extension);
		exit_message(MAP_NOT_BER);
	}
	free(extension);
}

void	get_coll_amount(t_data *data)
{
	int	i;
	int	j;
	int	coll_amount;

	i = 0;
	j = 0;
	coll_amount = 0;
	while (data->map[i])
	{
		while (data->map[i][j])
		{
			if (data->map[i][j] == 'C')
				coll_amount++;
			j++;
		}
		j = 0;
		i++;
	}
	data->coll_amount = coll_amount;
}

char	*strjoin_free(char *s1, char *s2)
{
	char	*line;

	line = ft_strjoin(s1, s2);
	free(s1);
	return (line);
}
