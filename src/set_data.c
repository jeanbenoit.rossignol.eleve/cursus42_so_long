/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_data.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:19:04 by jrossign          #+#    #+#             */
/*   Updated: 2022/09/30 18:19:41 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	set_data(char *map_name, t_data *data)
{
	int	i;

	i = 0;
	data->map = get_map(map_name);
	data->width = ft_strlen(data->map[0]);
	while (data->map[i])
		i++;
	if (i < 3)
		exit_message_free(data, MAP_NOT_OPEN);
	data->height = i;
	data->move = 0;
}

void	find_player(t_data *data)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (data->map[i])
	{
		while (data->map[i][j])
		{
			if (data->map[i][j] == 'P')
			{
				data->player_position[0] = i;
				data->player_position[1] = j;
				return ;
			}
			j++;
		}
		j = 0;
		i++;
	}
}
