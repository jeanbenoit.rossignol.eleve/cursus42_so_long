/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:19:24 by jrossign          #+#    #+#             */
/*   Updated: 2022/10/05 11:07:27 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

int	main(int argc, char **argv)
{
	t_data	data;

	if (argc != 2)
		printf("You can only pass a map to the game.\n");
	else
	{
		set_data(argv[1], &data);
		check_map_valid(&data);
		find_player(&data);
		flood_fill(&data);
		start_game(&data);
		free_data(&data);
	}
	return (0);
}
