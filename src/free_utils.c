/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:16:21 by jrossign          #+#    #+#             */
/*   Updated: 2022/09/27 19:16:24 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	free_data(t_data *data)
{
	int	i;

	i = 0;
	if (data->map)
	{
		while (data->map[i])
			free(data->map[i++]);
		free(data->map);
	}
}

void	free_map_copy(t_data *data)
{
	int	i;

	i = 0;
	if (data->map_copy)
	{
		while (data->map_copy[i])
			free(data->map_copy[i++]);
		free(data->map_copy);
	}
}
