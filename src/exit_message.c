/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_message.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:13:40 by jrossign          #+#    #+#             */
/*   Updated: 2022/09/30 17:59:20 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	exit_message(char *error)
{
	ft_putstr_fd(ERROR_BACKSLASH, 2);
	ft_putstr_fd(error, 2);
	exit(EXIT_FAILURE);
}

void	exit_message_free(t_data *data, char *error)
{
	ft_putstr_fd(ERROR_BACKSLASH, 2);
	ft_putstr_fd(error, 2);
	free_data(data);
	exit(EXIT_FAILURE);
}

void	exit_message_char(t_data *data, char c)
{
	ft_putstr_fd(ERROR_BACKSLASH, 2);
	if (c == 'P')
		ft_putstr_fd(MAP_NO_START, 2);
	else if (c == 'E')
		ft_putstr_fd(MAP_NO_EXIT, 2);
	else if (c == 'C')
		ft_putstr_fd(MAP_NO_COLL, 2);
	free_data(data);
	exit(EXIT_FAILURE);
}

int	exit_free(t_data *data)
{
	mlx_destroy_image(data->mlx_data->mlx, data->mlx_data->coll);
	mlx_destroy_image(data->mlx_data->mlx, data->mlx_data->exit);
	mlx_destroy_image(data->mlx_data->mlx, data->mlx_data->floor);
	mlx_destroy_image(data->mlx_data->mlx, data->mlx_data->player);
	mlx_destroy_image(data->mlx_data->mlx, data->mlx_data->wall);
	mlx_destroy_window(data->mlx_data->mlx, data->mlx_data->mlx_window);
	free_data(data);
	exit(EXIT_SUCCESS);
	return (EXIT_SUCCESS);
}
