/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves_utils_1.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:18:40 by jrossign          #+#    #+#             */
/*   Updated: 2022/09/30 17:59:48 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	set_new_pos(t_data *data, int pos_x, int pos_y)
{
	data->map[pos_x][pos_y] = 'P';
	data->player_position[0] = pos_x;
	data->player_position[1] = pos_y;
	data->move++;
	set_window(data);
}

void	set_coll(t_data *data, int pos_x, int pos_y)
{
	data->map[pos_x][pos_y] = 'P';
	data->player_position[0] = pos_x;
	data->player_position[1] = pos_y;
	data->move++;
	data->coll_amount--;
	set_window(data);
}

void	handle_exit(t_data *data)
{
	if (data->coll_amount != 0)
		printf("Go collect the coins!\n");
	else
	{
		data->move++;
		printf("Number of move: %i\n", data->move);
		exit_free(data);
	}
}
