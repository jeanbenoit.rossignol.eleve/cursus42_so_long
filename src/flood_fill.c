/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flood_fill.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:15:36 by jrossign          #+#    #+#             */
/*   Updated: 2022/10/05 09:35:48 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

// void	print_copy(t_data *data)
// {
// 	int	i;
//
// 	i = 0;
// 	printf("map_copy: \n");
// 	while (data->map_copy[i])
// 	{
// 		printf("%s\n", data->map_copy[i]);
// 		i++;
// 	}
// }

void	flood_fill(t_data *data)
{
	copy_map(data);
	flood_fill_algo(data, data->player_position[0], data->player_position[1]);
	check_after_fill(data);
	free_map_copy(data);
}

void	check_exit(t_data *data, int x, int y)
{
	if (data->map_copy[x][y + 1] == 'x')
		return ;
	if (data->map_copy[x][y - 1] == 'x')
		return ;
	if (data->map_copy[x + 1][y] == 'x')
		return ;
	if (data->map_copy[x - 1][y] == 'x')
		return ;
	else
		exit_message_free(data, MAP_NOT_OPEN);
}

void	check_after_fill(t_data *data)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (data->map_copy[i])
	{
		while (data->map_copy[i][j])
		{
			if (ft_strchr("C", data->map_copy[i][j]))
			{
				free_map_copy(data);
				exit_message_free(data, MAP_NOT_OPEN);
			}
			if (ft_strchr("E", data->map_copy[i][j]))
				check_exit(data, i, j);
			j++;
		}
		j = 0;
		i++;
	}
}

void	flood_fill_algo(t_data *data, int x, int y)
{
	if (ft_strchr("0C", data->map_copy[x][y + 1]))
	{
		data->map_copy[x][y + 1] = 'x';
		flood_fill_algo(data, x, y + 1);
	}
	if (ft_strchr("0C", data->map_copy[x + 1][y]))
	{
		data->map_copy[x + 1][y] = 'x';
		flood_fill_algo(data, x + 1, y);
	}
	if (ft_strchr("0C", data->map_copy[x][y - 1]))
	{
		data->map_copy[x][y - 1] = 'x';
		flood_fill_algo(data, x, y - 1);
	}
	if (ft_strchr("0C", data->map_copy[x - 1][y]))
	{
		data->map_copy[x - 1][y] = 'x';
		flood_fill_algo(data, x - 1, y);
	}
	return ;
}

void	copy_map(t_data *data)
{
	int		i;
	char	**map_copy;

	i = 0;
	map_copy = ft_calloc(data->height + 1, sizeof(char *));
	while (i < data->height)
	{
		map_copy[i] = ft_strdup(data->map[i]);
		i++;
	}
	data->map_copy = map_copy;
}
