/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_valid.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:17:08 by jrossign          #+#    #+#             */
/*   Updated: 2022/09/30 18:18:29 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	check_map_valid(t_data *data)
{
	check_valid_char(data);
	check_map_rectangle(data);
	check_map_closed(data);
	check_char(data, 'C');
	check_char(data, 'E');
	check_char(data, 'P');
}

void	check_valid_char(t_data *data)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (data->map[i])
	{
		while (data->map[i][j])
		{
			if (!ft_strchr(VALID_CHAR, data->map[i][j]))
				exit_message_free(data, OTHER_CHAR);
			j++;
		}
		j = 0;
		i++;
	}
}

void	check_char(t_data *data, char c)
{
	int	i;
	int	j;
	int	char_counter;

	i = 0;
	j = 0;
	char_counter = 0;
	while (data->map[i])
	{
		while (data->map[i][j])
		{
			if (data->map[i][j] == c)
				char_counter++;
			j++;
		}
		j = 0;
		i++;
	}
	if (char_counter == 0)
		exit_message_char(data, c);
	else if (c == 'P' && char_counter != 1)
		exit_message_free(data, MAP_TOO_MANY_P);
	else if (c == 'E' && char_counter != 1)
		exit_message_free(data, MAP_TOO_MANY_E);
}

void	check_map_rectangle(t_data *data)
{
	int		i;
	size_t	fix_length;

	i = 1;
	fix_length = ft_strlen(data->map[0]);
	if (fix_length < 3)
		exit_message_free(data, MAP_NOT_OPEN);
	while (data->map[i])
	{
		if (ft_strlen(data->map[i]) != fix_length)
			exit_message_free(data, MAP_NOT_RECTANGLE);
		i++;
	}
}

void	check_map_closed(t_data *data)
{
	int	i;

	i = 0;
	while (data->map[0][i])
	{
		if (data->map[0][i] != '1' || data->map[data->height - 1][i] != '1')
			exit_message_free(data, MAP_NOT_CLOSE);
		i++;
	}
	i = 0;
	while (data->map[i])
	{
		if (data->map[i][0] != '1' || data->map[i][data->width - 1] != '1')
			exit_message_free(data, MAP_NOT_CLOSE);
		i++;
	}
}
