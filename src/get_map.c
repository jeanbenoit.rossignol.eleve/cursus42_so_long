/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:16:31 by jrossign          #+#    #+#             */
/*   Updated: 2022/10/05 11:07:35 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	check_space(char *line)
{
	int	i;

	i = 0;
	while (line[i])
	{
		if (line[i] == '\n' && line[i + 1] == '\n')
		{
			free(line);
			exit_message(MAP_SPACE);
		}
		i++;
	}
}

void	check_dir(char *map_name)
{
	int	fd;

	fd = open(map_name, O_DIRECTORY);
	if (fd != -1)
	{
		close(fd);
		exit_message(MAP_IS_DIR);
	}
}

char	**get_map(char *map_name)
{
	char	*line;
	char	*buffer;
	char	**table_map;
	int		fd;

	check_valid_extension(map_name);
	line = ft_calloc(sizeof(char), 10000);
	buffer = ft_calloc(sizeof(char), 10001);
	check_dir(map_name);
	fd = open(map_name, O_RDONLY);
	if (fd == -1)
		exit_message(MAP_NO_FOUND);
	while (read(fd, buffer, 10000) != 0)
		line = strjoin_free(line, buffer);
	check_space(line);
	table_map = ft_split(line, '\n');
	free(buffer);
	free(line);
	close(fd);
	if (!table_map)
		exit_message(MAP_NOT_OPEN);
	return (table_map);
}
