/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:17:40 by jrossign          #+#    #+#             */
/*   Updated: 2022/09/30 17:59:44 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	move_up(t_data *data)
{
	int	pos_x;
	int	pos_y;

	pos_x = data->player_position[0];
	pos_y = data->player_position[1];
	if (data->map[pos_x - 1][pos_y] == '0')
	{
		data->map[pos_x][pos_y] = '0';
		set_new_pos(data, pos_x - 1, pos_y);
	}
	else if (data->map[pos_x - 1][pos_y] == 'C')
	{
		data->map[pos_x][pos_y] = '0';
		set_coll(data, pos_x - 1, pos_y);
	}
	else if (data->map[pos_x - 1][pos_y] == 'E')
		handle_exit(data);
	else
		return ;
}

void	move_down(t_data *data)
{
	int	pos_x;
	int	pos_y;

	pos_x = data->player_position[0];
	pos_y = data->player_position[1];
	if (data->map[pos_x + 1][pos_y] == '0')
	{
		data->map[pos_x][pos_y] = '0';
		set_new_pos(data, pos_x + 1, pos_y);
	}
	else if (data->map[pos_x + 1][pos_y] == 'C')
	{
		data->map[pos_x][pos_y] = '0';
		set_coll(data, pos_x + 1, pos_y);
	}
	else if (data->map[pos_x + 1][pos_y] == 'E')
		handle_exit(data);
	else
		return ;
}

void	move_right(t_data *data)
{
	int	pos_x;
	int	pos_y;

	pos_x = data->player_position[0];
	pos_y = data->player_position[1];
	if (data->map[pos_x][pos_y + 1] == '0')
	{
		data->map[pos_x][pos_y] = '0';
		set_new_pos(data, pos_x, pos_y + 1);
	}
	else if (data->map[pos_x][pos_y + 1] == 'C')
	{
		data->map[pos_x][pos_y] = '0';
		set_coll(data, pos_x, pos_y + 1);
	}
	else if (data->map[pos_x][pos_y + 1] == 'E')
		handle_exit(data);
	else
		return ;
}

void	move_left(t_data *data)
{
	int	pos_x;
	int	pos_y;

	pos_x = data->player_position[0];
	pos_y = data->player_position[1];
	if (data->map[pos_x][pos_y - 1] == '0')
	{
		data->map[pos_x][pos_y] = '0';
		set_new_pos(data, pos_x, pos_y - 1);
	}
	else if (data->map[pos_x][pos_y - 1] == 'C')
	{
		data->map[pos_x][pos_y] = '0';
		set_coll(data, pos_x, pos_y - 1);
	}
	else if (data->map[pos_x][pos_y - 1] == 'E')
		handle_exit(data);
	else
		return ;
}
