/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_game.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 19:20:49 by jrossign          #+#    #+#             */
/*   Updated: 2022/10/05 09:41:57 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

int	move_key(int keycode, t_data *data)
{
	if (keycode == 2)
		move_right(data);
	if (keycode == 13)
		move_up(data);
	if (keycode == 1)
		move_down(data);
	if (keycode == 0)
		move_left(data);
	if (keycode == 53)
		exit_free(data);
	printf("Number of move: %i\n", data->move);
	return (0);
}

void	start_game(t_data *data)
{
	get_coll_amount(data);
	set_mlx(data);
	set_window(data);
	mlx_key_hook(data->mlx_data->mlx_window, move_key, data);
	mlx_hook(data->mlx_data->mlx_window, 17, 0, exit_free, data);
	mlx_loop(data->mlx_data->mlx);
}

void	set_mlx(t_data *data)
{
	t_mlx	*mlx_data;

	mlx_data = malloc(sizeof(t_mlx) * 1);
	mlx_data->img_s = IMG_SIZE;
	mlx_data->mlx = mlx_init();
	mlx_data->coll = mlx_xpm_file_to_image(mlx_data->mlx, COLL,
			&mlx_data->img_s, &mlx_data->img_s);
	mlx_data->exit = mlx_xpm_file_to_image(mlx_data->mlx, EXIT,
			&mlx_data->img_s, &mlx_data->img_s);
	mlx_data->wall = mlx_xpm_file_to_image(mlx_data->mlx, WALL,
			&mlx_data->img_s, &mlx_data->img_s);
	mlx_data->floor = mlx_xpm_file_to_image(mlx_data->mlx, FLOOR,
			&mlx_data->img_s, &mlx_data->img_s);
	mlx_data->player = mlx_xpm_file_to_image(mlx_data->mlx, HERO,
			&mlx_data->img_s, &mlx_data->img_s);
	mlx_data->mlx_window = mlx_new_window(mlx_data->mlx, data->width * IMG_SIZE,
			data->height * IMG_SIZE, "so_long");
	data->mlx_data = mlx_data;
}

void	set_window(t_data *data)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < data->height)
	{
		while (j < data->width)
		{
			add_img(data->mlx_data, data->map[i][j], j, i);
			j++;
		}
		j = 0;
		i++;
	}
}

void	add_img(t_mlx *mlx_data, char c, int x, int y)
{
	if (c == '1')
		mlx_put_image_to_window(mlx_data->mlx, mlx_data->mlx_window,
			mlx_data->wall, x * IMG_SIZE, y * IMG_SIZE);
	else if (c == '0')
		mlx_put_image_to_window(mlx_data->mlx, mlx_data->mlx_window,
			mlx_data->floor, x * IMG_SIZE, y * IMG_SIZE);
	else if (c == 'C')
		mlx_put_image_to_window(mlx_data->mlx, mlx_data->mlx_window,
			mlx_data->coll, x * IMG_SIZE, y * IMG_SIZE);
	else if (c == 'E')
		mlx_put_image_to_window(mlx_data->mlx, mlx_data->mlx_window,
			mlx_data->exit, x * IMG_SIZE, y * IMG_SIZE);
	else if (c == 'P')
		mlx_put_image_to_window(mlx_data->mlx, mlx_data->mlx_window,
			mlx_data->player, x * IMG_SIZE, y * IMG_SIZE);
}
